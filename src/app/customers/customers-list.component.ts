import {Component, OnInit, ViewChild} from '@angular/core';
import {CustomerService} from '../services/customer.service';
import {Customer} from '../models/customer';
import {MatSort, MatTableDataSource} from '@angular/material';

@Component({
  selector: 'app-customers-list',
  templateUrl: './customers-list.component.html',
  styleUrls: ['./customers-list.component.scss']
})
export class CustomersListComponent implements OnInit {
  customers: Customer[];
  dataSource;
  displayedColumns = ['name', 'username', 'website', 'phone', 'company'];
  @ViewChild(MatSort) sort: MatSort;
  constructor(private customerService: CustomerService) {
  }

  ngOnInit() {
    this.customers = this.customerService.customers;
    // this.customerService.getCustomers().subscribe(data => {
      // this.customers = data;
      this.dataSource = new MatTableDataSource(this.customers);
      this.dataSource.sort = this.sort;
    // });
  }

  applyFilter(filterValue: string){
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
}
